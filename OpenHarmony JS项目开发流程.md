## 一、配置OpenHarmony开发环境

### 1.1软件需求

1）下载并安装好DevEco Studio 2.1 Release及以上版本，下载链接：https://developer.harmonyos.com/cn/develop/deveco-studio#download

 

2）获取OpenHarmony SDK包并解压，下载链接：https://mirror.iscas.ac.cn/OpenHarmony/sdk/OpenHarmony-SDK-2.0-Canary.7z

 

### 1.2配置OpenHarmony SDK

在DevEco主界面，点击工具栏中的File> Settings > Appearance & Behavior > System Settings > HarmonyOS SDK界面，点击HarmonyOS SDK Location加载SDK，如图1.2.1、1.2.2、1.2.3、1.2.4所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/093845_29d111fe_8496150.png "clip_image001.png")

<center>图1.2.1</center>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/093858_73d37169_8496150.png "clip_image003.png")

<center>图1.2.2</center>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/093908_2bfccc68_8496150.png "clip_image005.png")

<center>图1.2.3</center>



![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/093922_df224e70_8496150.png "clip_image007.png")

<center>图1.2.4</center>

之后一路Next、Finish完成配置。

### 1.3安装额外包

​	进入**OpenHarmony-SDK-2.0-Canary\js\2.2.0.0\build-tools\ace-loader**目录，然后在该目录下运行命令行工具，分别执行如下命令，直至安装完成，如图1.3.1所示：

```
npm cache clean -f

npm install
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/093934_8c692476_8496150.png "clip_image009.png")

<center>图1.3.1</center>

​	至此，OpenHarmony 开发环境配置完成。

## 二、创建、打开OpenHarmony JS工程

### 2.1 使用DevEco2.1 Release版本

​    2.1版本的DevEco针对OpenHarmony应用开发，只能通过导入Sample工程的方式来创建一个新工程，目前，支持OpenHarmony应用开发的Sample工程，请选择导入含有**“This sample is intended for novices at developing OpenHarmony applications.”**说明的Sample，例如选择common分类中的JsHelloWorld。如图2.1.1、2.1.2所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/093942_887411ae_8496150.png "clip_image011.png")

<center>图2.1.1</center>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/093949_85e58767_8496150.png "clip_image013.png")

<center>图2.1.2

​	Sample导入后，请打开工程下的build.gradle，修改hap插件的版本号为**“2.4.4.3-RC”**，如图2.1.3所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/094000_60ef80d3_8496150.png "clip_image015.png")

<center>图2.1.3

​	最后执行gradle sync，等待同步完成。同步成功后，便可以进行OpenHarmony应用开发了。

### 2.2 使用DevEco2.2 beta版本

  2.2版本的DevEco可以在菜单栏选择**File > New > New Project**来创建一个OpenHarmony工程，与HarmonyOS工程的创建方法一致。 

 

## 三、配置签名证书，编译

​	在完成应用开发后，需要编译并打包为Hap才能够安装到OpenHarmony开发板中。并且没有配置证书的Hap无法被安装到OpenHarmony开发板中，所以在在打包Hap前首先需要配置证书。

### 3.1 申请证书

​	OpenHarmony与HarmonyOS的证书不通用，所以需要额外进行申请，具体申请流程可以参考：[https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/%E9%85%8D%E7%BD%AEOpenHarmony%E5%BA%94%E7%94%A8%E7%AD%BE%E5%90%8D%E4%BF%A1%E6%81%AF.md](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/配置OpenHarmony应用签名信息.md)

 

### 3.2 配置证书

​	OpenHarmony与HarmonyOS的证书的配置方法相同，同样可以参考: [https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/%E9%85%8D%E7%BD%AEOpenHarmony%E5%BA%94%E7%94%A8%E7%AD%BE%E5%90%8D%E4%BF%A1%E6%81%AF.md](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/配置OpenHarmony应用签名信息.md)

 

### 3.3 编译

​    配置好证书后，就可以编译了。OpenHarmony与HarmonyOS的应用的编译方法相同。在工具栏点击**Build->Build Hap(s)/APP(s)->Build Hap(s)**，如图3.3.1所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/094012_a9d135d0_8496150.png "clip_image017.png")

<center>图3.3.1

​    等待项目编译完成，图3.3.2为编译成功的显示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/094025_35f6f8c7_8496150.png "clip_image019.png")

<center>图3.3.2

在工程文件的build目录下找到**XXXXXX-signed.hap**，这个就是可以在开发板中安装的Hap包，如图3.3.3所示。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/094033_f1b3f0e8_8496150.png "clip_image021.png")

<center>图3.3.3

## 四、推送到开发板/设备

### 4.1下载hdc_std工具

与HarmonyOS设备使用的hdc工具不同，OpenHarmony需要hdc_std作为调试工具，hdc-std下载链接：https://gitee.com/openharmony/developtools_hdc_standard

### 4.2 配置环境变量（Windows）

​    将**hdc_std.exe**添加到系统的环境变量中。在cmd中输入**”hdc_std -h”**验证环境变量是否添加成功。

### 4.2 使用hdc_std安装Hap

使用usb连接开发板后，使用**”hdc_std install <file_path>”**来安装在3.3节中编译好的Hap，如图4.2.1所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/094045_9185fc9f_8496150.png "clip_image023.png")

<center>图4.2.1

 

## 五、运行

以Hi3516DV300开发板为例，在开发板屏幕上点击应用图标即可打开应用。

 